<?php

namespace SpipLeague\Bridge\Filesystem;

use SpipLeague\Contracts\Filesystem\FilesystemInterface;
use SpipLeague\Contracts\Filesystem\LockFactoryInterface;
use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;

class Filesystem implements FilesystemInterface
{
    private readonly SymfonyFilesystem $filesystem;

    private readonly LockFactoryInterface $lockFactory;

    public function __construct(
        ?string $lockPath = \null,
        private readonly int $mode = 0777,
        private readonly int $umask = 0000,
    ) {
        $this->filesystem = new SymfonyFilesystem();
        $this->lockFactory = new LockFactory($lockPath);
    }

    /**
     * @codeCoverageIgnore
     */
    public function remove(string|iterable $files): void
    {
        $this->filesystem->remove($files);
    }

    /**
     * @codeCoverageIgnore
     */
    public function rename(string $origin, string $target, bool $overwrite = false): void
    {
        $this->filesystem->rename($origin, $target, $overwrite);
    }

    /**
     * @codeCoverageIgnore
     */
    public function mkdir(string|iterable $dirs, ?int $mode = \null): void
    {
        $this->filesystem->mkdir($dirs, $mode ?? $this->mode);
    }

    public function size(string $file): int
    {
        if (\is_readable($file)) {
            \clearstatcache(true, $file);

            return \filesize($file) ?: 0;
        }

        return 0;
    }

    /**
     * @codeCoverageIgnore
     */
    public function exists(string|iterable $files): bool
    {
        return $this->filesystem->exists($files);
    }

    /**
     * @codeCoverageIgnore
     */
    public function touch(string|iterable $files, ?int $time = null, ?int $atime = null): void
    {
        $this->filesystem->touch($files, $time, $atime);
    }

    public function write(string $file, string $content): bool
    {
        $ok = false;
        $lock = $this->lockFactory->createLock($file);
        if ($lock->acquire(true)) {
            try {
                $this->filesystem->dumpFile($file, $content);
                $ok = true;
            } catch (\Throwable) {
                $ok = false;
            }

            $lock->release();
        }

        return $ok;
    }

    public function read(string $file): string
    {
        $content = '';
        $lock = $this->lockFactory->createLock($file);

        if ($lock->acquireRead()) {
            if ($this->filesystem->exists($file)) {
                $content = (string) \file_get_contents($file);
            }

            $lock->release();
        }

        return $content;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getLockFactory(): LockFactoryInterface
    {
        return $this->lockFactory;
    }

    public function mtime(string $file): ?int
    {
        if (!$this->exists($file)) {
            return null;
        }

        return \filemtime($file) ?: 0;
    }

    /**
     * @codeCoverageIgnore
     */
    public function chmod(
        string|iterable $files,
        ?int $mode = \null,
        ?int $umask = \null,
        bool $recursive = false,
    ): void {
        $this->filesystem->chmod($files, $mode ?? $this->mode, $umask ?? $this->umask, $recursive);
    }
}
