<?php

namespace SpipLeague\Bridge\Filesystem;

use SpipLeague\Contracts\Filesystem\LockFactoryInterface;
use Symfony\Component\Lock\LockFactory as SymfonyLockFactory;
use Symfony\Component\Lock\SharedLockInterface;
use Symfony\Component\Lock\Store\FlockStore;

class LockFactory implements LockFactoryInterface
{
    private readonly SymfonyLockFactory $decorated;

    public function __construct(?string $lockPath = \null)
    {
        $store = new FlockStore($lockPath);
        $this->decorated = new SymfonyLockFactory($store);
    }

    public function createLock(string $resource, ?float $ttl = 300.0, bool $autoRelease = true): SharedLockInterface
    {
        return $this->decorated->createLock($resource, $ttl, $autoRelease);
    }
}
