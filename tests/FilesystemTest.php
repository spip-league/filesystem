<?php

namespace SpipLeague\Test\Bridge\Filesystem;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Bridge\Filesystem\Filesystem;
use SpipLeague\Bridge\Filesystem\LockFactory;

#[CoversClass(Filesystem::class)]
#[CoversClass(LockFactory::class)]
class FilesystemTest extends TestCase
{
    /**
     * @return array<string,array{expected:int,file:string}>
     */
    public static function dataSize(): array
    {
        return [
            'exist' => [
                'expected' => 14,
                'file' => __DIR__ . '/fixtures/14chars',
            ],
            'not-exist' => [
                'expected' => 0,
                'file' => 'whatever',
            ],
        ];
    }

    #[DataProvider('dataSize')]
    public function testSize(int $expected, string $file): void
    {
        // Given
        $fs = new Filesystem();

        // When
        $actual = $fs->size($file);

        // Then
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{expected:bool,file:string}>
     */
    public static function dataWrite(): array
    {
        return [
            'can' => [
                'expected' => true,
                'file' => '/tmp/can',
            ],
            'cannot' => [
                'expected' => false,
                'file' => '/opt/cannot',
            ],
        ];
    }

    #[DataProvider('dataWrite')]
    public function testWrite(bool $expected, string $file): void
    {
        // Given
        $fs = new Filesystem();

        // When
        $actual = $fs->write($file, 'test');

        // Then
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{expected: string, file: string}>
     */
    public static function dataRead(): array
    {
        return [
            'exist' => [
                'expected' => '14 characters' . "\n",
                'file' => __DIR__ . '/fixtures/14chars',
            ],
            'not-exist' => [
                'expected' => '',
                'file' => 'whatever',
            ],
        ];
    }

    #[DataProvider('dataRead')]
    public function testRead(string $expected, string $file): void
    {
        // Given
        $fs = new Filesystem();

        // When
        $actual = $fs->read($file);

        // Then
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{expected:int|null|false,file:string}>
     */
    public static function dataMtime(): array
    {
        return [
            'not exists' => [
                'expected' => null,
                'file' => 'whatever',
            ],
            'exists' => [
                'expected' => \filemtime('tests/fixtures/14chars'), // 1686581317,
                'file' => 'tests/fixtures/14chars',
            ],
        ];
    }

    #[DataProvider('dataMtime')]
    public function testMtime(int|null|false $expected, string $file): void
    {
        // Given
        $fs = new Filesystem();

        // When
        $actual = $fs->mtime($file);

        // Then
        $this->assertEquals($expected, $actual);
    }
}
