# spip-league/filesystem

Fonctions d'accès et de manipulation de fichiers.

Verrou intégré en lecture et écriture de fichier.

Nécessite PHP>=8.2 (Donc SPIP5)

## Installation

```bash
composer require spip-league/filesystem
```

## Usage

```php
use SpipLeague\Component\Filesystem\Filesystem;

$fs = new Filesystem();

$size = $fs->exists('path/to/file') ? $fs->size('path/to/file') : 0;

$fs->rename('source.file', 'destination.file');

$fs->remove('path/to/file');
```

```php
use SpipLeague\Component\Filesystem\Filesystem;
use SpipLeague\Contracts\FilesystemInterface;

function lambda(FilesystemInterface $fs)
{
    // $file = ...

    $size = $fs->size($file);

    // ...
}

lambda(new Filesystem);
```

## Tests

```bash
# All tests
docker run --rm -v $(pwd):/build/app spip/tools:8.2 clean phplint analyze cs test
```
