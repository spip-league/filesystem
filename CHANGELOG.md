# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 3.0.0 - 2024-12-14

### Added

- Méthode `chmod(string|iterable $files)`
- provide `spip-league\filesystem-implementation`

### Changed

- require `spip-league\filesystem-contracts`
- 3.0.x-dev

### Removed

- `SpipLeague\Bridge\Filesystem\FilesystemInterface`

## 2.0.1 - 2024-11-30

### Added

- Compat PHP 8.4

### Changed

- up `symfony/filesystem` to 7.2

## 2.0.0 - 2024-07-18

### Changed

- namespace PHP de `Spip` à `SpipLeague`
- PHP 8.2 mini

### Added

- Méthode `mtime(string $file)` pour récupérer la date de modificaton au format timestamp

## 1.1.0 - 2024-02-01

### Added

- Exposer la LockFactory
